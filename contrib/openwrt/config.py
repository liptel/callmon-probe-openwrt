#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pyuci.pyuci import uci
import os

class Config(object):
    host                        = uci.get("callmon","global","host")
    port                        = uci.get("callmon","global","port")
    zabbix_login                = uci.get("callmon","global","zabbix_login")
    zabbix_password             = uci.get("callmon","global","zabbix_password")
    https			= uci.get("callmon","global","https")
    if https == 'True' or https == 'On':
    	zabbix_url                  = 'https://%s/zabbix' % host
    else:
    	zabbix_url                  = 'http://%s/zabbix' % host
    server_ssh_login_username   = uci.get("callmon","global","server_ssh_login_username")
    tree_base                   = uci.get("callmon","global","tree_base")
    base_dir                    = '%s/python/'                              % tree_base
    reference_wav               = '%s/wavs/test_ulaw_25s.wav'               % tree_base
    rsa_key_path                = uci.get("callmon","global","rsa_key_path")
    recorded_dir                = uci.get("callmon","global","recorded_dir")
    asterisk_ami_user           = uci.get("callmon","global","asterisk_ami_user")
    asterisk_ami_secret         = uci.get("callmon","global","asterisk_ami_secret")
    probe_com_interface_name    = uci.get("callmon","global","probe_com_interface_name")
    server_wav_dir              = uci.get("callmon","global","server_wav_dir")
    wav_archive                 = uci.get("callmon","global","wav_archive")
    probe_template_name         = uci.get("callmon","global","probe_template_name")
    cron_analyze_times          = uci.get("callmon","global","cron_analyze_times")
    cron_call_times             = uci.get("callmon","global","cron_call_times")
    platform                    = 'OWRT' if os.path.isfile('/etc/openwrt_version') else 'OTHER'
    template_extconf            = '/usr/share/callmon/templates/asterisk/extensions.conf'
    extconf                     = '/etc/asterisk/extensions.conf'
    template_managerconf        = '/usr/share/callmon/templates/asterisk/manager.conf'
    managerconf                 = '/etc/asterisk/manager.conf'
    template_sipconf            = '/usr/share/callmon/templates/asterisk/sip.conf'
    sipconf                     = '/etc/asterisk/sip.conf'
    log_base                    = '/var/log/callmon/'
