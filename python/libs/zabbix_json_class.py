#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from pprint import pprint
import json
import sys

# Interface to zabbix with auth
class ZabbixJson(object):
    def __init__(self, cfg):
        self.Cfg            = cfg
        self.url            = self.Cfg.zabbix_url + '/api_jsonrpc.php'          # url to rpc api
        self.Session        = None                                              # session variable to store the ids after successful auth
        self.Headers        = {'content-type': 'application/json',}             # http headers
        self.Active_items   = []                                                # list of items for server
        self.auth()                                                             # when the class is instantiated, the authentication is performed


    # json encapsulation and sending to server, id is hardcoded so far
    def request(self, method, params):
        payload = {
            "jsonrpc" : "2.0",
            "method" : method,
            "params": params,
            "auth" : self.Session,
            "id" : 2,
        }
        result  = requests.post(self.url, data=json.dumps(payload), headers=self.Headers, verify=False)
        result = result.json()
        return  result

        #response = self.Jsn.request(method='template.get', params={"filter": {"name": "Probe"}})
        #probeid = json.loads(response)['result'][0]['templateid']
        #return probeid

    # reads the templateid for the creation of the new probe on the zabbix server
    def get_probe_templateid(self):
        name = self.Cfg.probe_template_name
        response = self.request(method='template.get', params={"filter": {"name": name}})
        probeid = response['result'][0]['templateid']
        return probeid


    # login credentials sent to zabbix
    def auth(self):
        result = self.request("user.login",{
                    'user': self.Cfg.zabbix_login,
                    'password': self.Cfg.zabbix_password,
                 })
        if hasattr(result, 'error') == False:
            self.Session = result['result']
            #print "Connected to zabbix server"
            return True
        else:
            print "ERROR ",result['error']['code']," - ",result['error']['data'];
            return False

    # set the probe state to active
    def activate_host(self, ip):
        result = self.request("host.get",{'output': ['hostid'],'filter':{'name':ip} })
        try:
            id = result['result'][0]['hostid']
        except IndexError:
            id = 0
        if id != 0:
            res2 = self.request("host.update",{'hostid':id ,'status':0})
            return True
        else:
            return False

    # get list of callmon probes
    def get_hosts(self, atributes):
        result = self.request("host.get",{'output': atributes,'filter':{'status':[0]} }) #'extend'
        return result['result']

    def get_host_id(self, host):
        result = self.request("host.get",{
            "output":['id'],
            "filter":{'host':[host]}
        })
        try:
            id = result['result'][0]['hostid']
        except IndexError:
            id = 0
        return id

    # check if probe exists already
    def check_host(self, name):
        result = self.request("host.get",{
		"output": "count",
		"filter": {
			"host": [name]}
			}
		)
        if len(result['result']) == 1:		# there is exactly one probe with this name, therefore it exists
            return True
        else:
            return False

    # add probe to the list on server, see controller create_probe function
    def add_host(self, params):
        result = self.request("host.create", params)
        return result

    # get the probe history, not yet tested
    def get_history(self):
        result = self.request("history.get",  {
            "history": 4,
            "itemids": [self.Active_items[0]['itemid']],
            "output":"extend"})


    def add_item(self, name, host):
        host_id = self.get_host_id(host)
        if host_id != 0:
            result = self.request("item.create",{
                "name": name,
                "key_": name,
                "hostid": host_id,
                "type": 7,
                "value_type": 4, # can be type 0, but it is rounded to 2 decimals
                "interfaceid": "11",
                "applications": [],
                "delay": 30,
                "status": 0
            })
        else:
            print "Host not found"


    def check_item(self, name, host):
        result = self.request("item.get",{
             "output": "count",
	     "host": host,
	     "search": {
                 "key_": name
		 }
        })
        try:
            ret = result['result']
        except AttributeError:
            ret = False
        return True if len(ret)==1 else False
