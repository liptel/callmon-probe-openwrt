#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pprint                             import pprint
from callmon.libs.ssh_class             import Ssh
from callmon.libs.zabbix_json_class     import ZabbixJson
from callmon.libs.acl_class             import AsteriskCallList
from callmon.libs.asterisk_ami_class    import AstConnection
import uuid
import re
import os
import datetime
import netifaces
import itertools
import subprocess
import shutil

## Class for managing and initialization other objects, using their functions and interaction between them.
class Controller(object):
    """
    Main class used to control the functionalities of the probe.
    """
    ## Creating of objects using inside controller
    def __init__(self, cfg):
        self.Cfg    = cfg
        self.Jsn    = ZabbixJson(cfg)
        self.ip     = self.get_ip()
        self.mac    = self.get_mac()

    def run_cmd(self, cmd=None, args=[], outfile=None, errfile=None, stdin=None):
        status = None
        if cmd:
            if self.Cfg.platform == 'OWRT':
                if cmd == 'sudo':
                    cmd=args[0]
		    args=args[1:]
		#else: pass

	    #sys.exit()
            popen_cmd = [cmd] + args
            try:
                if outfile and errfile:
                    p = subprocess.Popen(
                            popen_cmd,
                            stdin=subprocess.PIPE if stdin else None,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE
                            )
                    if stdin:
                        stdout, stderr = p.communicate(stdin)
                    else:
                        stdout, stderr = p.communicate()
                    status = p.returncode
                    for fout, content in [(outfile,stdout), (errfile,stderr)]:
                        with open(fout, 'a') as f:
                            f.write(content)
                else:
                    if stdin:
                        p = subprocess.Popen(popen_cmd, stdin=subprocess.PIPE)
                        p.communicate(stdin)
                    else:
                        p = subprocess.Popen(popen_cmd)
                        p.wait()
                    status = p.returncode
            except OSError as e:
                print "ERROR: Execution of command %s failed: " % (cmd), e
        else:
            print "Command not specified."
        return status

    def init_ssh(self):
        # first repair the permissions on the private key to prevent error message
        self.run_cmd('chmod', args=[
            '0400',
            '/root/.ssh/id_rsa'
        ])
        # then log in to callmon server and add self to the list of hosts
        status = self.run_cmd('ssh', args=[
            '-oStrictHostKeyChecking=no',
            '%s@%s' % ( self.Cfg.server_ssh_login_username, self.Cfg.host),
            'callmons -u %s' %  self.mac,
            ])
        if status == 0:
            return True
        else:
            return False

    ## add task to cron table for same time in every hour
    def create_cron_from_string(self, string=None):
        """
        Create crontab from stdin, which consists of typical cron rules.
        """
        if string:
            self.run_cmd(cmd='crontab', args=['-'], stdin=string)
        else:
            print 'No string for cron addition specified.'

    ## remove all cron tasks from cron table
    def remove_all_crons(self):
        self.run_cmd(cmd='crontab', args=['-r'])

    ## start and enable cron
    def start_cron(self):
        self.run_cmd(cmd='/etc/init.d/cron', args=['start'])
        self.run_cmd(cmd='/etc/init.d/cron', args=['enable'])

    ## display actual cron table with all tasks
    def print_cron(self):
        self.run_cmd(cmd='crontab', args=['-l'])

    ## get own mac address of probe
    def get_mac(self):
        return str(hex(uuid.getnode()).lstrip("0x").rstrip("L")).zfill(12)

    ## get own ip address of probe
    def get_ip(self):
        return netifaces.ifaddresses(self.Cfg.probe_com_interface_name)[netifaces.AF_INET][0]['addr']

    ## check if exist probe with this mac address on zabbix server
    def probe_exist(self):
        return self.Jsn.check_host(self.mac);

    ## create new probe on zabbix server with ip and mac of this computer
    def create_probe(self):
        params = {
            "host": self.mac,
            "name": self.ip,
            "groups":[
                {
                    "groupid":4
                }
            ],
            "templates":[
                {
                    "templateid": self.Jsn.get_probe_templateid()
                }
            ],
            "interfaces":[
                {
                    "type":1,
                    "main":1,
                    "useip":1,
                    "ip":"127.0.0.1",
                    "dns":"",
                    "port":10050
                }
            ]
        }
        return self.Jsn.add_host(params)

    ## set status of probe to active
    def activate_probe(self):
        return self.Jsn.activate_host(self.ip)

    ## generating new sip.conf file with all neighbours of probe
    def generate_sip_conf(self):
        hosts = self.Jsn.get_hosts(['host','name'])
        items = ''
        try:
            with open(self.Cfg.template_sipconf, 'r') as f:
                content = f.read()
        except IOError as e:
            print e
            return False
        matches = re.findall( r'##FOREACH(.*?)FOREACH##', content, re.DOTALL) #(.*)FOREACH##
        for fc in matches:
            for host in hosts:
                if host['name'] != self.ip:
                    subcontent = fc.replace('##ip##',host['name'])
                    subcontent = subcontent.replace('##mac##',host['host']).replace('##my_mac##',self.mac)
                    items += subcontent

        content = content.replace(fc, items)
        content = content.replace("##FOREACH","")
        content = content.replace("FOREACH##","")

	# stop asterisk server before writing sip.conf
        self.run_cmd('sudo', args=['asterisk', '-rx', 'core stop now'])
        try:
            with open(self.Cfg.sipconf, 'w') as f:
                f.write(content)
        except IOError as e:
            print e
            return False
	# run asterisk after sip.conf has been generated
        self.run_cmd('sudo', args=['/etc/init.d/asterisk', 'start'])
        return True

    ## generating new extensions.conf file with all neighbours of probe
    def generate_extensions_conf(self):
        import uuid # only needed here, therefore importing here

        # stop asterisk server before writing extensions.conf
        import time
        time.sleep(2)
        self.run_cmd('sudo', args=['asterisk', '-rx', 'core stop now'])

        with open(self.Cfg.template_extconf, 'r') as f:
            content = f.read()

        ASTID = "MYIDENTITY => %s" % hex(uuid.getnode()).lstrip("0x").rstrip("L").zfill(12)
        FILETOPLAY = "FILETOPLAY => %s" % self.Cfg.reference_wav.rstrip('.wav')
        try:
            with open(self.Cfg.extconf, 'w') as f:
                content = content.replace('###ASTID###', ASTID)
                content = content.replace('###FILETOPLAY###', FILETOPLAY)
                f.write(content)

        except IOError as e:
            print e
            return False
        # run asterisk after extensions.conf has been generated
        self.run_cmd('sudo', args=['/etc/init.d/asterisk', 'start'])
        return True

    ## generating new manager.conf file with all neighbours of probe
    def generate_manager_conf(self):
        self.run_cmd('sudo', args=['asterisk', '-rx', 'core stop now'])

        with open(self.Cfg.template_managerconf, 'r') as f:
            content = f.read()
        try:
            with open(self.Cfg.managerconf, 'w') as f:
                content = content.replace('###AMIUSER###', self.Cfg.asterisk_ami_user)
                content = content.replace('###AMIPASS###', self.Cfg.asterisk_ami_secret)
                f.write(content)

        except IOError as e:
            print e
            return False
        # run asterisk after extensions.conf has been generated
        self.run_cmd('sudo', args=['/etc/init.d/asterisk', 'start'])
        return True

    ## load new files from probe to the server and than their backup to different directory
    def rsync(self):
        fileList = os.listdir(self.Cfg.recorded_dir)
        self.run_cmd(
                'rsync',
                args=['-a', self.Cfg.recorded_dir, self.Cfg.wav_archive],
                outfile='/dev/null'
                #errfile='/dev/null'
                )
        self.run_cmd(
                'rsync',
                args=[
                    '-av',
                    '--remove-source-files',
                    '--rsh=ssh -i %s' % self.Cfg.rsa_key_path,
                    self.Cfg.recorded_dir,
                    '%s@%s:%s' % (self.Cfg.server_ssh_login_username, self.Cfg.host, self.Cfg.server_wav_dir)
                    ],
                outfile='/dev/null'
                )
        return fileList

    ## generating new calls between half of known probes from topology
    def send_calls(self):
        ret                 = {'call_number':0, 'ok':0, 'error':0}
        list_of_hosts       = sorted([host['host'] for host in self.Jsn.get_hosts(['host'])])
        acl                 = AsteriskCallList(list_of_hosts, self.mac)
        local_acl           = acl.acl     # list of tuples [(origin, destination), ....]
        asc                 = AstConnection(self.Cfg)

        f = open(self.Cfg.log_base + 'call.log', "w")

        for item in local_acl:
            f.write( str(datetime.datetime.now())+' - Start call from %s to: Local/%s@local-loop \n' % (self.mac, item[1]))
            asc.connect()
            asc.send_action('login')
            if asc.read_response('Success'):
                asc.send_action(
                                action      = 'Originate',
                                attributes  = {
                                                'Channel' : 'Local/%s@local-loop' % item[1],
                                                'Context' : 'to-probe',
                                                'Exten'   : '%s' % item[1],
                                                'Priority': '1',
                                                'Timeout' : '30000'
                                                }
                                )
                print "Calling to:"+item[1]
                ret['call_number'] += 1
                if asc.read_response('Success'):
                    ret['ok'] += 1
                else:
                    ret['error'] += 1
            else:
                print "Can't connect to Asterisk"
                return False
        f.close()
        print "Statistics: "+str(ret)
        return ret

    ## restarting of asterisk service
    def asterisk_restart(self):
        self.run_cmd('/etc/init.d/asterisk', args=['restart'])

    ## runing analyzer script on server side for getting quality of call
    def run_analyzation(self, f):
        target_mac = self.get_target_mac(f)
        item = 'pesq-'+ target_mac
        item_exist = self.Jsn.check_item(item, self.mac) # if not exist item on this probe
        if item_exist != True and self.mac != target_mac:
            print "Add new item '"+item+"' on host "+self.ip
            self.Jsn.add_item(item, self.mac)   # Create new item

        self.Ssh = Ssh( self.Cfg, 'ssh')
        if self.Ssh.command(
                'callmons -a ' +
                ' ' +
                f +
                ' ' +
                self.mac
                ) != False :
            print "File added to processing queue on zabbix server"
            return True
        else:
            print "Error:Cannot add file to processing queue on server"
            return False

    ## parse name of recorded file and get target probe mac address
    def get_target_mac(self, file):
        parts = file.split('-')
        try:
            ret = parts[1]
        except IndexError:
            ret = False
        return ret

    ## create items from every probe to all anothers (all connections)
    def create_edges(self):
        hosts = self.Jsn.get_hosts(['host'])
        for item in hosts:
            for i in hosts:
                if item['host'] != i['host']:
                    print "create:"+item['host']+" "+i['host']
                    item_exist = self.Jsn.check_item('pesq-'+item['host'], i['host']) #IF not exist item on this probe
                    if item_exist != True:
                        self.Jsn.add_item( 'pesq-'+item['host'],i['host'])
